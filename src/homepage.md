# Welcome to the zoomsquare Style Guide

This styleguide was created for zoomsquare employees, but we hope it's helpful for content and communication teams too. 


## Structure
The structure of the framework builds on top of the guidelines for scalable and modular architecture. You can read more about it here: https://smacss.com/ 

1. Base - A base rule is applied to an element using an element selector along with any pseudo-class. It's defining the default styling.
2. Modules - A module is discrete component of a page e.g. navigation, buttons, modals, ...
3. Patterns - A pattern is something that augments and overrides all other styles. For example hiding or showing something.
4. ZS - These are modules that are built specially for zoomsquare and not in a general way.


## File naming

All folder names should be plural. All component filenames should be singular.

```css
/* Do */
@import 'button.scss';
@import 'font-size.scss';

/* Don't */
@import 'buttons.scss';
@import 'font-sizes.scss';
```


## Class naming

Each of our classes adhere to a naming convetion called BEM. You can read more about it here: https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/

```
.block {}
.block__element {}
.block--modifier {}
```

## Variables naming

The name of a variable should match its attribute and should be as descriptive as possible.

```
/* Do */
$h1-font-size
$box-background-color
$box-hover-background-color

/* Don't */
$h1-size
$box-bg-color
$hover-box-background-color
```

## Writing CSS

We follow the rules of Nicolas Gallagher: https://github.com/necolas/idiomatic-css 

```
.selector {
    /* Positioning */
    position: absolute;
    z-index: 10;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;

    /* Flexbox */
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;

    /* Display & Box Model */
    overflow: hidden;
    box-sizing: border-box;
    width: 100px;
    height: 100px;
    padding: 10px;
    border: 10px solid #333;
    margin: 10px;

    /* Other */
    background: #000;
    color: #fff;
    font-family: sans-serif;
    font-size: 16px;
    text-align: right;
}
```

We welcome any feedback for improving the guide: hello@zoomsquare.com