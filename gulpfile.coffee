gulp = require 'gulp'
browserSync = require("browser-sync").create()
kss = require 'kss'

$ = require('gulp-load-plugins')
  pattern: ['gulp-*', 'run-sequence', 'del']

options = 
  country: "at"
  dest:
    root: "public"

kssConfig = 
  source: ["scss/", "src/"]
  destination: "public/"
  css: ["screen.css"] 
  title: "zoomsquare Style Guide"

gulp.task "default", ()->
  $.runSequence "build", "browser-sync"

gulp.task "build", (cb)->
  $.runSequence "cleanup", "sass", "kss", "assets-shared", cb

gulp.task "cleanup", ()->
  $.del.sync "#{options.dest.root}"

gulp.task "browser-sync", ->
  gulp.start "watch"
  browserSync.init
    server:
      baseDir: "./public/"
      index: "index.html"
    online: false
    notify: false

gulp.task "sass", ->
  gulp.src './scss/screen.scss'
  .pipe $.plumber
    errorHandler: (err)->
      $.util.log err.toString()
      $.util.log $.util.colors.yellow("Waiting for file change\n")
      this.emit('end')
  .pipe $.sass({errLogToConsole: true,outputStyle: 'expanded'})
  .pipe $.autoprefixer()
  .pipe gulp.dest './public/'

gulp.task "watch", ()->
  $.watch ["./scss/**/*"], ()->
    $.runSequence "sass", "kss", browserSync.reload

gulp.task "kss", ()->
  kss kssConfig

require('web-basic/shared/gulp')(gulp,options)