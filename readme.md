# Setup

```
npm install
```

# Build & Run

```
gulp
```
will build sass, kss, add shared-assets from basic and serve everything with browsersync  
changes to sass will be watched and automatically displayed

# Remote

```
zoomsquare@utils.zoomsquare.com:zs-sass-framework.git
```

# Architecture

1. public: compiled framework
2. scss: the framework itself with all the scss files
3. src: documentation files

# Tags

We set tags to every imporant commit to make sure that every repo can stick to it's own version. Sometimes you might do changes which can cause problems or need markup changes. And you don't want to check every repo if it still works. And you also don't want to do additional work. So set your tag. Make sure you also push it. 
Then go to the packages file and set the correct tag. npm install and you're settled!

# Documentation

The documentation system which is used here is the node version of KSS, a widely spread style guide generator. You may inform yourself about KSS and it's syntax here: https://github.com/kss-node/kss-node

There are simply 2 things you have to do:

## Comment the SCSS
The first step is to add comments to your scss files in the scss folder right on the top. Its a) a benefit for everyone who looks into this files and wants to know how it is about and b) KSS needs it to generate the documentation. 

You add
1. name - button
2. description - all the buttons
3. modifying classes - .button--large, etc.
4. markup file - button.html
5. navigation - modules.button

## Markup
The second step is to add a corresponding markup file to your scss file where you write a little markup in it. For our button-example it will simply be:

```
<a class="button {{modifier_class}}">Button</a>
```

The {{modifier_class}} is also matching the modifier class you named in the comments of your file. If you look in the documentation of your button module you'll see all the different colours and sizes of the buttons we have. 